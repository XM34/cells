extends Node2D


const PLAYER_CELL = preload("res://src/PlayerCell.tscn")
const CELL = preload("res://src/Cell.tscn")

const GRID_SIZE = Vector2(50, 50)

const COLORS = [
	Color.blue,
	Color.orange,
	Color.blueviolet,
	Color.crimson
]


var player_cell_count = 0

signal player_death


func _process(delta):
	update()


remotesync func add_food_cell(name, pos):
	var cell = CELL.instance()
	cell.name = name
	cell.position = pos
	cell.color = get_random_color()
	$FoodCells.add_child(cell)

remotesync func remove_food_cell(name):
	for food_cell in $FoodCells:
		if food_cell.name == name:
			food_cell.queue_free()


remotesync func add_player(player_id, pos, size, color):
	var player = PLAYER_CELL.instance()
	$Players.add_child(player)
	
	player.name = str(player_id)
	player.position = pos
	player.size = size
	player.color = color
	player.set_network_master(player_id)
	
	if player_id == get_tree().get_network_unique_id():
		player_cell_count += 1
		player.connect("death", self, "_on_death")

remotesync func remove_player(player_id):
	# Todo simplyfy using get_node
	var player_node = $Players.get_node(str(player_id))
	if player_node != null:
		player_node.queue_free()

func get_player(player_id):
	return $Players.get_node(str(player_id))


func _on_death(killed_id, killer_id):
	player_cell_count -= 1
	if player_cell_count == 0:
		emit_signal("player_death")


func _draw():
	var line_count = 50
	var height = get_viewport().size.y
	var width = get_viewport().size.x
	var pixel = width/line_count
	
	# Get view rectangle
	var ctrans = get_canvas_transform()
	var start_pos = -ctrans.get_origin() / ctrans.get_scale()
	var view_size = get_viewport_rect().size / ctrans.get_scale()
	var end_pos = start_pos + view_size
	
	for x in range((GRID_SIZE.x * int(start_pos.x / GRID_SIZE.x)), (GRID_SIZE.x * int(end_pos.x / GRID_SIZE.x)), GRID_SIZE.x):
		draw_line(Vector2(x, start_pos.y), Vector2(x, end_pos.y), Color.gray)
	
	for y in range((GRID_SIZE.y * int(start_pos.y / GRID_SIZE.y)), (GRID_SIZE.y * int(end_pos.y / GRID_SIZE.y)), GRID_SIZE.y):
		draw_line(Vector2(start_pos.x, y), Vector2(end_pos.x, y), Color.gray)
	
	#var x = 0
	#while x < width:
	#	x += pixel
	#	draw_line(Vector2(x, 0), Vector2(x, 600), Color.gray)
		
	#var y = 0
	#while y < height:
	#	y += pixel
	#	draw_line(Vector2(0, y), Vector2(width, y), Color.gray)


func get_random_position():
	return Vector2(randi() % 2000 - 1000, randi() % 2000 - 1000)

func get_random_color():
	return COLORS[randi() % len(COLORS)]
