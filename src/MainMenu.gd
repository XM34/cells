extends MarginContainer

onready var root_menu = $TitleAlign/MenuAlign/RootMenu
onready var create_menu = $TitleAlign/MenuAlign/CreateGameMenu
onready var join_menu = $TitleAlign/MenuAlign/JoinGameMenu

onready var create_server_port = $TitleAlign/MenuAlign/CreateGameMenu/PortInput

onready var join_server_address = $TitleAlign/MenuAlign/JoinGameMenu/AddressInput
onready var join_server_port = $TitleAlign/MenuAlign/JoinGameMenu/PortInput

func _on_CreateGameButton_pressed():
	root_menu.hide()
	create_menu.show()


func _on_JoinGameButton_pressed():
	root_menu.hide()
	join_menu.show()


func _on_ExitGameButton_pressed():
	get_tree().quit()


func _on_BackButton_pressed():
	root_menu.show()
	create_menu.hide()
	join_menu.hide()


func _on_StartCreatedGameButton_pressed():
	# Setup server
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(int(create_server_port.text))
	get_tree().network_peer = peer
	
	get_tree().change_scene("res://src/GameManager.tscn")


func _on_StartJoinedGameButton_pressed():
	# Setup signal listener
	get_tree().connect("connected_to_server", self, "_on_joined_server", [], SceneTree.CONNECT_ONESHOT)
	
	# Join server
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(join_server_address.text, int(join_server_port.text))
	get_tree().network_peer = peer

func _on_joined_server():
	get_tree().change_scene("res://src/GameManager.tscn")
