extends "res://src/Cell.gd"

const BASE_MOVE_SPEED = 5
const ZOOM_MODIFIER = 0.05

const CAMERA_SCN = preload("res://src/PlayerCamera.tscn")

func _ready():
	rset_config("position", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	if name == str(get_tree().get_network_unique_id()):
		prepare()


func prepare():
	var camera = CAMERA_SCN.instance()
	camera.name = "Camera"
	add_child(camera)
	camera.make_current()
	connect("grow", self, "_on_grow")


func _process(delta):
	if Engine.editor_hint:
		return
	
	rpc("move")


master func move():
	var direction = get_local_mouse_position().normalized()
	translate(direction * (exp(-size) + BASE_MOVE_SPEED))
	rset_unreliable("position", position)


func _on_grow(size):
	var camera = get_node("Camera")
	var camera_tween = camera.get_node("Tween")
	
	camera_tween.interpolate_property(camera, "zoom", camera.zoom, (sqrt(size) * ZOOM_MODIFIER + 1.0), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	camera_tween.start()
