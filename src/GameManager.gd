extends Node


var food_cell_id = 1
var timer

func _ready():
	# Connection lost signal
	get_tree().connect("server_disconnected", self, "_on_disconnect")
	
	if get_tree().get_network_unique_id() == 1:
		# Client connected signal (only host)
		get_tree().connect("network_peer_connected", self, "_on_client_joined")
		get_tree().connect("network_peer_disconnected", self, "_on_client_left")
		
		# Start food cell timer
		timer = Timer.new()
		add_child(timer)
		
		timer.connect("timeout", self, "_on_spawn_food_cell")
		timer.set_wait_time(0.5)
		timer.set_one_shot(false)
		timer.start()


func _unhandled_input(event):
	if event.is_action_pressed("split_cell"):
		pass
	elif event.is_action_pressed("throw_food"):
		pass


func _on_disconnect():
	get_tree().change_scene("res://src/MainMenu.tscn")


func _on_client_joined(client_id):
	# Add other players to new client
	for player in $Playfield/Players.get_children():
		$Playfield.rpc_id(client_id, "add_player", int(player.name), player.position, player.size, player.color)
	
	# Add food cells to new client
	for food_cell in $Playfield/FoodCells.get_children():
		$Playfield.rpc_id(client_id, "add_food_cell", food_cell.name, food_cell.position)

func _on_client_left(client_id):
	$Playfield.rpc("remove_player", client_id)

func _on_PlayButton_pressed():
	var player_id = get_tree().get_network_unique_id()
	var player_name = $UiLayer/GameMenu/MenuPanel/VBox/NameInput.text
	
	if $UiLayer/GameMenu/MenuPanel/VBox/NameInput.text == "":
		return
	
	$UiLayer/GameMenu.hide()
	
	$Playfield.rpc(
		"add_player",
		player_id,
		$Playfield.get_random_position(),
		2,
		$Playfield.get_random_color()
	)

func _on_spawn_food_cell():
	$Playfield.rpc("add_food_cell", str(food_cell_id), $Playfield.get_random_position())
	food_cell_id += 1

func _on_death():
	$UiLayer/GameMenu.show()
