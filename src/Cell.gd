tool
extends RigidBody2D

const BASE_SIZE = 10
const GROTH_MODIFIER = 10
const EAT_MARGIN = 10

export(int) var size = 1 setget set_size
export(Color) var color

signal death(killed_id, killer_id)
signal grow(size)


func _ready():
	rset_config("size", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	update_collision()

remotesync func set_size(value):
	print("Set Size: ", value)
	size = value
	update()
	update_collision()
	emit_signal("grow", size)


func update_collision():
	if Engine.editor_hint:
		return
	if has_node("CollisionShape"):
		var contactShape = CircleShape2D.new()
		contactShape.radius = (sqrt(size) * GROTH_MODIFIER) + BASE_SIZE
		$CollisionShape.shape = contactShape

func _draw():
	draw_circle(Vector2.ZERO, (sqrt(size) * GROTH_MODIFIER) + BASE_SIZE, color)


func _on_Cell_body_entered(body):
	if body.size != null and body.name != null and get_tree().get_network_unique_id() == 1:
		if size < body.size:
			rpc("die", body.name)
		elif size > body.size:
			print("Size: ", (size + body.size))
			rset("size", (size + body.size))


remotesync func die(killer_id):
	emit_signal("death", [name, killer_id])
	queue_free()
